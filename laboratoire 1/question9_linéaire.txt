movi 1,0x00ff
movi 2,0x00ff
movi 3,0x0000
nop
movi 4,0x0000
nop
movi 5,0x8000
nop
movi 6,0x0010
nop

beq 0,1,end_prog
beq 0,2,end_prog

begin_loop: addi 6,6,-1
	nand 7,5,2
	nand 7,7,7

	beq 7,5,a
	beq 0,0,s

	a: nand 7,5,3
	nand 7,7,7
	add 3,3,1
	beq 7,5,msb1
	beq 0,0,msb0

	s: beq 0,6,end_prog
		nand 7,5,3
		nand 7,7,7

		add 3,3,3
		add 4,4,4

		beq 7,5,carry
		beq 0,0,end_loop

		carry: addi 4,4,1

	end_loop: add 2,2,2
		beq 0,0,begin_loop

end_prog: halt

msb1: nand 7,5,1
	nand 7,7,7
	beq 7,0,msb_diff
	addi 4,4,1
	beq 0,0,s

msb0: nand 7,5,1
	nand 7,7,7
	beq 7,5,msb_diff
	beq 0,0,s

msb_diff: nand 7,5,3
	nand 7,7,7
	beq 7,5,s
	addi 4,4,1
	beq 0,0,s




