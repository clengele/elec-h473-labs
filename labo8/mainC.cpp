/*
The program detects edges border from a raw input image .

It takes as argument the raw image (input) and the dimensions of the input image (width, height).
It outputs the edges borders detected via a max min edge detector.

Program was made for the course ELEC-H473 - Microprocessor Architecture

The routine for max min edge detection is made with C.

April 2021
authors :
    Antoine Bertoux
    Celestin Lengele
    Vlad Stefan
    Sacha Verheeke
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void minMaxC(unsigned char* src, unsigned char* dst, int W, int H, int kernel_radius);
    // Declaration of the assembly routine for any kernel size

int main(int argc, char** argv)
{
	// === Initialisation === //
    int W, H; // Width, Height of the raw image
    char* img_path; // path of the input image

    // == Error Handling == //
    if(argc != 4) //Ensure that the right number of options is given
    {
        fprintf(stderr, "Incorrect use of the program : %s\n", argv[0]);
        fprintf(stderr, "The correct use is : %s <input> <width> <height>", argv[0]);
        return 1;
    }


    img_path = argv[1];
    const char* substring = "_out_C_3x3.raw";
    char out_path_3[strlen(img_path)+10];
    memcpy(out_path_3, img_path, strlen(img_path)-4);
    memcpy(out_path_3 + strlen(img_path) - 4, substring, 14); //The output name of the file for the 3x3 kernel is initilized
    printf("%s\n", out_path_3);

    substring = "_out_C_5x5.raw";
    char out_path_5[strlen(img_path)+10];
    memcpy(out_path_5, img_path, strlen(img_path)-4);
    memcpy(out_path_5 + strlen(img_path) - 4, substring, 14); //The output name of the file for the 5x5 kernel is initilized
    printf("%s\n", out_path_5);

    substring = "_out_C_7x7.raw";
    char out_path_7[strlen(img_path)+10];
    memcpy(out_path_7, img_path, strlen(img_path)-4);
    memcpy(out_path_7 + strlen(img_path) - 4, substring, 14); //The output name of the file for the 7x7 kernel is initilized
    printf("%s\n", out_path_7);

    // == Error Handling == //
    if(sscanf(argv[2], "%d", &W) == EOF || sscanf(argv[3], "%d", &H) ==  EOF || W < 0 || H < 0)
    {
        fprintf(stderr, "Both width and height should be positive integers");
        return 1;
    }


    // === END Initialisation === //


    // === Begin Core Program === //

    // Image size fixed (or from header)
    unsigned char *src; // Pointers to source array
    unsigned char *dst, *dst1, *dst2; // Pointers to destination arrays
                                        // Each destination array for each kernel (3x3, 5x5, 7x7)
    fprintf(stdout,"Time 3X3 (s), Time 5x5 (s), Time 7x7 (s)\n");
    clock_t start, end;
    float time;
    
    // == Local Min Max == //

    for(int b = 0; b<20; b++)
    {
        // -- Arrays Initialisation -- //
	    src = (unsigned char *) malloc (W * H * sizeof(unsigned char));
	    dst = (unsigned char *) calloc (W * H, sizeof(unsigned char));
	    dst1 = (unsigned char *) calloc (W * H, sizeof(unsigned char));
	    dst2 = (unsigned char *) calloc (W * H, sizeof(unsigned char));

	    // Check if enough memory, although with virtual memory less like to happen
	    if (src == NULL || dst == NULL) {
	        printf ("Out of memory!");
	        exit (1);
	    }

	    // fp1 reading | fp2, fp3, fp4 writing
	    FILE *fp1 = fopen(img_path,"rb");
	    FILE *fp2 = fopen(out_path_3,"wb");
	    FILE *fp3 = fopen(out_path_5,"wb");
	    FILE *fp4 = fopen(out_path_7,"wb");

	    if (fp1 == NULL) {
	    	// and we close the pointer
	    	printf("Cannot open specified file!");
	        exit(1);
	    }
	    if(fread(src, sizeof(u_int8_t), W*H, fp1) == 0) exit(1);

        // ------------------ 3x3 kernel | fp2 file ------------------ //
        start = clock(); 								// START BENCHMARK
        minMaxC(src, dst, W, H, 1);
	    end = clock(); 									// END BENCHMARK

	    time = (float) (end - start)/CLOCKS_PER_SEC;	// Time spent
	    fprintf(stdout, "%f, ", time); 				    // PRINT BENCHMARK

	    fwrite(dst, sizeof(unsigned char), W * H, fp2);
        free(dst);
        fclose(fp2);
        // ----------------------------------------------------------- //

        // ------------------ 5x5 kernel | fp3 file ------------------ //
        start = clock(); 								// START BENCHMARK
        minMaxC(src, dst1, W, H, 2);
	    end = clock(); 									// END BENCHMARK

	    time = (float) (end - start)/CLOCKS_PER_SEC;	// Time spent
	    fprintf(stdout,"%f, ", time); 				    // PRINT BENCHMARK

	    fwrite(dst1, sizeof(unsigned char), W * H, fp3);
        free(dst1);
        fclose(fp3);
        // ----------------------------------------------------------- //

        // ------------------ 7x7 kernel | fp4 file ------------------ //
	    start = clock(); 								// START BENCHMARK
        minMaxC(src, dst2, W, H, 3);
	    end = clock(); 									// END BENCHMARK

	    time = (float) (end - start)/CLOCKS_PER_SEC;	// Time spent
	    fprintf(stdout, "%f\n", time); 				    // PRINT BENCHMARK

	    fwrite(dst2, sizeof(unsigned char), W * H, fp4);
	    free(dst2);
	    fclose(fp4);
	    // ----------------------------------------------------------- //


        // free source and close read image
	    free(src); // avoid memory leaks
	    fclose(fp1);



    }

    return 0;
}


void minMaxC(unsigned char* src, unsigned char* dst, int W, int H, int kernel_radius)
{
    // Declaration of the assembly routine for any kernel size
    int pmax, pmin, pval;
    for (int i =kernel_radius ; i<W-kernel_radius; i++)
    {
        for(int j = kernel_radius; j<H-kernel_radius; j++)
        {
            pmax = 0;
            pmin = 255;
            for(int pi = i-kernel_radius; pi <= i + kernel_radius; pi ++)
            {
                for(int pj = j-kernel_radius; pj <= j +kernel_radius; pj++)
                {
                    pval = src[pi +  W*pj];

                    if(pval < pmin)
                        pmin = pval;
                    if(pval > pmax)
                        pmax = pval;
                }
            }
            dst[i + j*W] = pmax - pmin;
        }
    }


}
// === END Program === //
