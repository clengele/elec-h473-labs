

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(int argc, char** argv)
{
    int W, H;
    int ret_val;
    unsigned int threshold = 128;
    char* img_path;
    if(argc < 4 || argc > 5)
    {
        fprintf(stderr, "Incorrect use of the program : %s\n", argv[0]);
        fprintf(stderr, "The correct use is : %s <input> <width> <height> [threshold = 128]", argv[0]);
        return 1;
    }
    else if(argc == 5)
    {
        ret_val = sscanf(argv[4], "%ud", &threshold);
        if(ret_val == EOF)
        {
            fprintf(stderr, "The treshold argument is supposed to be a number.\n");
            fprintf(stderr, "Incorrect use of the program : %s\n", argv[0]);
            fprintf(stderr, "The correct use is : %s <input> <dest> <width> <height> <threshold>", argv[0]);
            return 1;
        }
        if(threshold < 0 || threshold > 255)
        {
            threshold = 128;
            fprintf(stderr, "WARNING : The treshold is supposed to be an integer between 0 and 255\n");
            fprintf(stderr, "The received integer will be positive modulo 255");
        }
    }

    img_path = argv[1];
    const char* substring = "out_SIMD.raw";
    char out_path[strlen(img_path)+8];
    memcpy(out_path, img_path, strlen(img_path)-4);
    memcpy(out_path + strlen(out_path) - 5, substring, 12);

    if(sscanf(argv[2], "%d", &W) == EOF || sscanf(argv[3], "%d", &H) ==  EOF || W < 0 || H < 0)
    {
        fprintf(stderr, "Both width and height should be positive integers");
        return 1;
    }




     // Image size fixed (or from header)
    unsigned char *src; // Pointers to arrays
    unsigned char *dst;
    threshold -= 128;


    for(int j=0; j<20; j++)
    {
    src = (unsigned char *) malloc (W*H*sizeof(unsigned char));
    dst = (unsigned char *) malloc (W*H*sizeof(unsigned char));

    // Check if enough memory, although with virtual memory less like to happen
    if (src == NULL || dst == NULL) {
        printf ("Out of memory!");
        exit (1);
    }
    FILE *fp1 = fopen(img_path,"rb");
    FILE *fp2 = fopen(out_path,"wb");
    if (fp1 != NULL) {
    // we read data if file opened
        fread(src, sizeof(u_int8_t), W*H, fp1);
    } // and we close the pointer
    else {
        printf("Cannot open specified file!");
        exit(1);
    }

    int threshold_shift = 128;

    clock_t start, end;
    int iter = W*H/16;
    float time;
    //AJOUTER UN FOR POUR LE BENCHMARK ICI


        start = clock();

        __asm__(
        "mov %[out], %%rsi\n"
        "mov %[in] , %%rax\n"
        "mov %[it] , %%rcx\n"
        "movdqu %[t],   %%xmm7\n"
        "vpbroadcastb %%xmm7 ,%%xmm7\n"
        "movdqu %[ts] , %%xmm3\n" // tresh in xmm3
        "vpbroadcastb %%xmm3, %%xmm3\n"

        "loop: \n"
            "movdqa (%%rax), %%xmm0 \n" //mov input in xmm0
            "psubb %%xmm3, %%xmm0\n" // xmm3 = -128 - -input
            "pcmpgtb %%xmm7, %%xmm0 \n"

            "movdqa %%xmm0, (%%rsi) \n" // input -> output
            "add $16, %%rsi\n"
            "add $16, %%rax\n"
            "subw $1, %[it] \n"
            "jnz loop; \n"
        :
        : [out]"m" (dst), [in]"m" (src), [it]"m" (iter), [t]"m" (threshold), [ts]"m" (threshold_shift)
        :
        );


        end = clock();
        time = (float)(end - start)/CLOCKS_PER_SEC;
        printf("Time spent: %f\n", time);




    fwrite(dst,sizeof(unsigned char),W*H,fp2);
    free(src); // we do this to avoid memory leaks
    free(dst);

    fclose(fp1);
    fclose(fp2);
    }
    return EXIT_SUCCESS;
}

