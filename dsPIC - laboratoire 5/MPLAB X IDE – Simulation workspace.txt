Instructions :

You can configure your environment to suit your needs:
– Launch the MPLAB X IDE.
– Open the project “h473 dsPIC.X” (File > Open Project...) which already includes:
– the choice of the microcontroller.
– the configuration of the debugger in the simulation mode which enables you to execute
and debug the code in your development environment, without any microcontroller hard-
ware.
– Display the following windows in your IDE
– C source code
– Output for note, warning and error messages
– Disassembly listing (Window > Debugging > Output > Disassembly Listing File or
Window > Debugging > Disassembly, but the later requires the simulation to be started
in debugging mode first)
– Stopwatch: number of cycles required to execute each instruction
– Watch window to observe the registers and variables
– File Register: to dump the Data Memory