movi 1,0x00ff
movi 2,0x0000
movi 3,0x0000
nop
movi 4,0x0000
nop
movi 5,0x8000
nop

nand 6,1,5
nand 6,6,6 					// MSB reg1 dans reg6

beq 0,1,end
loop: beq 0,2,end 			// While reg2 ≠ 0

	nand 7,3,5
	nand 7,7,7 				// MSB reg3 dans reg7
	
	add 3,3,1

	beq 6,7,same 			// MSB reg1 = MSB reg2
							// MSB reg1 ≠ MSB reg2
	nand 7,3,5
	nand 7,7,7 				// MSB reg3 (solution) dans reg7

	beq 7,0,carry			// MSB reg3 = 0
	beq 0,0,endloop			// MSB reg3 = 1

	same: beq 6,5,carry 	// MSB reg1 = MSB reg2 = 1
		beq 0,0,endloop		// MSB reg1 = MSB reg2 = 0

	carry: addi 4,4,1

	endloop: addi 2,2,-1
	beq 0,0,loop

end: halt