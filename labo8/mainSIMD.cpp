/*
The program detects edges border from a raw input image .

It takes as argument the raw image (input) and the dimensions of the input image (width, height).
It outputs the edges borders detected via a max min edge detector.

Program was made for the course ELEC-H473 - Microprocessor Architecture

The routine for max min edge detection is made with inline assembly.

April 2021
authors :
    Antoine Bertoux
    Celestin Lengele
    Vlad Stefan
    Sacha Verheeke
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void minMaxSIMD3(unsigned char* src, unsigned char* dst, int W, int H); // Declaration of the assembly routine for kernel of 3x3 size
void minMaxSIMD5(unsigned char* src, unsigned char* dst, int W, int H); // Declaration of the assembly routine for kernel of 5x5 size
void minMaxSIMD7(unsigned char* src, unsigned char* dst, int W, int H); // Declaration of the assembly routine for kernel of 7x7 size
// There are 3 declarations of the assembly routines because a generic assembly routine (in function of the kernel size) was not implemented
// instead 3 hardcoded routines were implemented

int main(int argc, char** argv)
{
	// === Initialisation === //
	int W, H; // Width, Height of the raw image
    char* img_path; // path of the input image

    if(argc != 4) //Ensure that the right number of options is given
    {
        fprintf(stderr, "Incorrect use of the program : %s\n", argv[0]);
        fprintf(stderr, "The correct use is : %s <input> <width> <height>", argv[0]);
        return 1;
    }


    img_path = argv[1];
    const char* substring = "_out_SIMD_3x3.raw";
    char out_path_3[strlen(img_path)+14];
    memcpy(out_path_3, img_path, strlen(img_path)-4);
    memcpy(out_path_3 + strlen(img_path) - 4, substring, 18); //The output name of the file for the 3x3 kernel is initilized

    substring = "_out_SIMD_5x5.raw";
    char out_path_5[strlen(img_path)+14];
    memcpy(out_path_5, img_path, strlen(img_path)-4);
    memcpy(out_path_5 + strlen(img_path) - 4, substring, 18); //The output name of the file for the 5x5 kernel is initilized

    substring = "_out_SIMD_7x7.raw";
    char out_path_7[strlen(img_path)+14];
    memcpy(out_path_7, img_path, strlen(img_path)-4);
    memcpy(out_path_7 + strlen(img_path) - 4, substring, 18); //The output name of the file for the 7x7 kernel is initilized


    // == Error Handling == //
    if(sscanf(argv[2], "%d", &W) == EOF || sscanf(argv[3], "%d", &H) ==  EOF || W < 0 || H < 0)
    {
        fprintf(stderr, "Both width and height should be positive integers");
        return 1;
    }


	// === Begin Core Program === //
     // Image size fixed (or from header)
    unsigned char *src; // Pointers to source array
    unsigned char *dst, *dst1, *dst2; // Pointers to destination arrays
                                        // Each destination array for each kernel (3x3, 5x5, 7x7)
    fprintf(stdout,"Time SIMD 3X3 (s), Time SIMD 5x5 (s), Time SIMD 7x7 (s)\n");
    clock_t start, end;
    float time;

    // == Local Min Max == //

    for(int b = 0; b<20; b++){

        // -- Arrays Initialisation -- //

		src = (unsigned char *) malloc (W*H*sizeof(unsigned char));
		dst = (unsigned char *) calloc (W * H , sizeof(unsigned char));
		dst1 = (unsigned char *) calloc (W * H, sizeof(unsigned char));
	    dst2 = (unsigned char *) calloc (W * H, sizeof(unsigned char));


		// Check if enough memory, although with virtual memory less like to happen
		if (src == NULL || dst == NULL) {
		    printf ("Out of memory!");
		    exit (1);
		}

		// fp1 reading | fp2, fp3, fp4 writing
		FILE *fp1 = fopen(img_path,"rb");
		FILE *fp2 = fopen(out_path_3,"wb");
	    FILE *fp3 = fopen(out_path_5,"wb");
	    FILE *fp4 = fopen(out_path_7,"wb");

		if (fp1 == NULL) {
            // and we close the pointer
            printf("Cannot open specified file!");
            exit(1);
        }
        if(fread(src, sizeof(u_int8_t), W*H, fp1) == 0) exit(1);

        // ------------------ 3x3 kernel | fp2 file ------------------ //
        start = clock(); 								// START BENCHMARK
        minMaxSIMD3(src, dst, W, H);
	    end = clock(); 									// END BENCHMARK

	    time = (float) (end - start)/CLOCKS_PER_SEC;	// Time spent
	    fprintf(stdout, "%f, ", time); 				    // PRINT BENCHMARK

	    fwrite(dst, sizeof(unsigned char), W * H, fp2);
        free(dst);
        fclose(fp2);
        // ----------------------------------------------------------- //

        // ------------------ 5x5 kernel | fp3 file ------------------ //
        start = clock(); 								// START BENCHMARK
        minMaxSIMD5(src, dst1, W, H);
	    end = clock(); 									// END BENCHMARK

	    time = (float) (end - start)/CLOCKS_PER_SEC;	// Time spent
	    fprintf(stdout,"%f, ", time); 				    // PRINT BENCHMARK

	    fwrite(dst1, sizeof(unsigned char), W * H, fp3);
        free(dst1);
        fclose(fp3);
        // ----------------------------------------------------------- //

        // ------------------ 7x7 kernel | fp4 file ------------------ //
	    start = clock(); 								// START BENCHMARK
        minMaxSIMD7(src, dst2, W, H);
	    end = clock(); 									// END BENCHMARK

	    time = (float) (end - start)/CLOCKS_PER_SEC;	// Time spent
	    fprintf(stdout, "%f\n", time); 				    // PRINT BENCHMARK

	    fwrite(dst2, sizeof(unsigned char), W * H, fp4);
	    free(dst2);
	    fclose(fp4);
	    //-----------------------------------------------------------//

        // free source and close read image
	    free(src); // avoid memory leaks
	    fclose(fp1);
    }
    return EXIT_SUCCESS;
}

void minMaxSIMD3(unsigned char* src, unsigned char* dst, int W, int H)
{
    // Declaration of the assembly routine for kernel of 3x3 size

    long int iter = (W)*(H-2)/14;
    unsigned char *src1, *src2;
    src1 = src + W;
    src2 = src1 + W; // give a pointer to first and second line
    dst += W;
    __asm__(
            "mov %[out], %%rsi\n"				// rsi -> output
            "mov %[in] , %%rax\n"				// rax -> first input line
            "mov %[in1],  %%rbx\n"              // rbx -> second input line
            "mov %[in2],  %%rdi\n"              // rdi -> third input line
            "mov %[it], %%r12\n"
            ""                                        // rax, rbx, rdi are the 3 lines to use local min-max with SIMD with 3x3 kernels
                                                // mov 16 bytes by 16 bytes

            "loop: \n"
                "movdqu (%%rax), %%xmm0\n"      // Charges the lines in the SIMD vector
                "movdqu (%%rbx), %%xmm1\n"
                "movdqu (%%rdi), %%xmm2\n"

                "movdqu %%xmm0, %%xmm4\n"       // Copies xmm0 in xmm4

                "pmaxub %%xmm1, %%xmm0\n"       // Takes the maximum values in the columns and stores it in xmm0
                "pmaxub %%xmm2, %%xmm0\n"

                "pminub %%xmm1, %%xmm4 \n"      // Takes the minimum values in the columns and stores it in xmm4
                "pminub %%xmm2, %%xmm4 \n"

                                                // -----------------------
                "movdqa %%xmm0, %%xmm1\n"       // Two copies of xmm0 in xmm1 and xmm2
                "movdqa %%xmm0, %%xmm2\n"


                "psrldq $1,  %%xmm1\n"          // Shifts
                "psrldq $2,  %%xmm2\n"

                "pmaxub %%xmm1, %%xmm0\n"       // Takes the maximum values in the entire kernel and stores it in xmm0
                "pmaxub %%xmm2, %%xmm0\n"
                                                // -----------------------

                                                // -----------------------
                                                // Applies the same previous steps to get the minimum in the entire kernel
                "movdqa %%xmm4, %%xmm1\n"
                "movdqa %%xmm4, %%xmm2\n"



                "psrldq $1,  %%xmm1\n"
                "psrldq $2,  %%xmm2\n"



                "pminub %%xmm1, %%xmm4\n"
                "pminub %%xmm2, %%xmm4\n"
                                                // -----------------------
                "psubb  %%xmm4, %%xmm0\n"       // Applies max - min
                "movdqu %%xmm0, (%%rsi)\n"      // Writes on the output

                "add $14, %%rsi\n"				// Passes to the next 14 bytes of inputs and output
                "add $14, %%rax\n"
                "add $14, %%rbx\n"
                "add $14, %%rdi\n"

                "sub $1, %%r12 \n"	            // iterator -= 1
                "jnz loop \n"					// jump to loop label if iterator > 0
            :
            : [out]"m" (dst), [in]"m" (src), [it]"m" (iter),[in1]"m" (src1), [in2]"m" (src2)
            : "rsi", "rax", "rbx", "rdi", "xmm0", "xmm1", "xmm2", "xmm4", "r12"
            );
}

void minMaxSIMD5(unsigned char* src, unsigned char* dst, int W, int H)
{
    // Declaration of the assembly routine for kernel of 5x5 size

    long int iter = (W)*(H-4)/12;
    unsigned char *src1, *src2, *src3, *src4;
    src1 = src + W;
    src2 = src1 + W;
    src3 = src2 + W;
    src4 = src3 + W; // give a pointer to first and second line
    dst += 2*W;

    __asm__(
            "mov %[out],  %%rsi\n"              // rsi -> output
            "mov %[in] ,  %%rax\n"				// rax -> first input line
            "mov %[in1],  %%rbx\n"              // rbx -> second input line
            "mov %[in2],  %%rdi\n"              // rdi -> third input line
            "mov %[in3],  %%r8\n"               // r8 -> fourth input line
            "mov %[in4],  %%r9\n"               // r9 -> fifth input line
            "mov %[it], %%r12\n"
                                                    // rax, rbx, rdi, r8, r9 are the 5 lines to use local min-max with SIMD with 5x5 kernels
                                                // mov 16 bytes by 16 bytes

            "loop3: \n"
                "movdqu (%%rax), %%xmm0\n"      // Charges the lines in the SIMD vector
                "movdqu (%%rbx), %%xmm1\n"
                "movdqu (%%rdi), %%xmm2\n"
                "movdqu (%%r8), %%xmm3\n"
                "movdqu (%%r9), %%xmm4\n"

                "movdqu %%xmm0, %%xmm5\n"       // Copies xmm0 in xmm5

                "pmaxub %%xmm1, %%xmm0\n"       // Takes the maximum values in the columns and stores it in xmm0
                "pmaxub %%xmm2, %%xmm0\n"
                "pmaxub %%xmm3, %%xmm0\n"
                "pmaxub %%xmm4, %%xmm0\n"

                "pminub %%xmm1, %%xmm5 \n"      // Takes the minimum values in the columns and stores it in xmm5
                "pminub %%xmm2, %%xmm5 \n"
                "pminub %%xmm3, %%xmm5 \n"
                "pminub %%xmm4, %%xmm5 \n"
                                                // -----------------------
                "movdqa %%xmm0, %%xmm1\n"       // Two copies of xmm0 in xmm1 and xmm2
                "movdqa %%xmm0, %%xmm2\n"
                "movdqa %%xmm0, %%xmm3\n"
                "movdqa %%xmm0, %%xmm4\n"

                "psrldq $1,  %%xmm1\n"          // Shifts
                "psrldq $2,  %%xmm2\n"
                "psrldq $3,  %%xmm3\n"
                "psrldq $4,  %%xmm4\n"

                "pmaxub %%xmm1, %%xmm0\n"       // Takes the maximum values in the entire kernel and stores it in xmm0
                "pmaxub %%xmm2, %%xmm0\n"
                "pmaxub %%xmm3, %%xmm0\n"
                "pmaxub %%xmm4, %%xmm0\n"
                                                // -----------------------
                                                // Applies the same previous steps to get the minimum in the entire kernel
                "movdqa %%xmm5, %%xmm1\n"
                "movdqa %%xmm5, %%xmm2\n"
                "movdqa %%xmm5, %%xmm3\n"
                "movdqa %%xmm5, %%xmm4\n"



                "psrldq $1,  %%xmm1\n"
                "psrldq $2,  %%xmm2\n"
                "psrldq $3,  %%xmm3\n"
                "psrldq $4,  %%xmm4\n"

                "pminub %%xmm1, %%xmm5\n"
                "pminub %%xmm2, %%xmm5\n"
                "pminub %%xmm3, %%xmm5\n"
                "pminub %%xmm4, %%xmm5\n"
                                                // -----------------------
                "psubb  %%xmm5, %%xmm0\n"       // Applies max - min
                "movdqu %%xmm0, (%%rsi)\n"      // Writes on the output

                "add $12, %%rsi\n"		// Passes to the next 14 bytes of inputs and output
                "add $12, %%rax\n"
                "add $12, %%rbx\n"
                "add $12, %%rdi\n"
                "add $12, %%r8\n"
                "add $12, %%r9\n"

                "sub $1, %%r12 \n"	               // iterator -= 1
                "jnz loop3 \n"					// jump to loop label if iterator > 0
            :
            : [out]"m" (dst), [in]"m" (src), [it]"m" (iter),[in1]"m" (src1), [in2]"m" (src2), [in3]"m" (src3), [in4]"m" (src4)
            : "rsi", "rax", "rbx", "rdi", "r8", "r9", "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "r12"
            );
}

void minMaxSIMD7(unsigned char* src, unsigned char* dst, int W, int H)
{
    // Declaration of the assembly routine for kernel of 7x7 size

    long int iter = (W)*(H-6)/10;
    unsigned char *src1, *src2, *src3, *src4, *src5, *src6;
    src1 = src + W;
    src2 = src1 + W;
    src3 = src2 + W;
    src4 = src3 + W; // give a pointer to first and second line
    src5 = src4 + W;
    src6 = src5 + W;
    dst += 3*W;
   __asm__(
            "mov %[out],  %%rsi\n"				// rsi -> output
            "mov %[in] ,  %%rax\n"				// rax -> input
            "mov %[in1],  %%rbx\n"              // rbx -> second input line
            "mov %[in2],  %%rdi\n"              // rdi -> third input line
            "mov %[in3],  %%r8\n"               // r8 -> fourth input line
            "mov %[in4],  %%r9\n"               // r9 -> fifth input line
            "mov %[in5],  %%r10\n"              // r10 -> sixth input line
            "mov %[in6],  %%r11\n"              // r11 -> seventh input line
            "mov %[it], %%r12\n"
                                                    // rax, rbx, rdi, r8, r9, r10, r11 are the 7 lines to use local min-max with SIMD with 7x7 kernels
                                                // mov 16 bytes by 16 bytes

            "loop7: \n"
                "movdqu (%%rax), %%xmm0\n"      // Charges the lines in the SIMD vector
                "movdqu (%%rbx), %%xmm1\n"
                "movdqu (%%rdi), %%xmm2\n"
                "movdqu (%%r8), %%xmm3\n"
                "movdqu (%%r9), %%xmm4\n"
                "movdqu (%%r10), %%xmm5\n"
                "movdqu (%%r11), %%xmm6\n"


                "movdqu %%xmm0, %%xmm7\n"       // Copies xmm0 in xmm7

                "pmaxub %%xmm1, %%xmm0\n"       // Takes the maximum values in the columns and stores it in xmm0
                "pmaxub %%xmm2, %%xmm0\n"
                "pmaxub %%xmm3, %%xmm0\n"
                "pmaxub %%xmm4, %%xmm0\n"
                "pmaxub %%xmm5, %%xmm0\n"
                "pmaxub %%xmm6, %%xmm0\n"

                "pminub %%xmm1, %%xmm7 \n"      // Takes the minimum values in the columns and stores it in xmm7
                "pminub %%xmm2, %%xmm7 \n"
                "pminub %%xmm3, %%xmm7 \n"
                "pminub %%xmm4, %%xmm7 \n"
                "pminub %%xmm5, %%xmm7 \n"
                "pminub %%xmm6, %%xmm7 \n"
                                                // -----------------------
                "movdqa %%xmm0, %%xmm1\n"       // Two copies of xmm0 in xmm1 and xmm2
                "movdqa %%xmm0, %%xmm2\n"
                "movdqa %%xmm0, %%xmm3\n"
                "movdqa %%xmm0, %%xmm4\n"
                "movdqa %%xmm0, %%xmm5\n"
                "movdqa %%xmm0, %%xmm6\n"

                "psrldq $1,  %%xmm1\n"          // Shifts
                "psrldq $2,  %%xmm2\n"
                "psrldq $3,  %%xmm3\n"
                "psrldq $4,  %%xmm4\n"
                "psrldq $5,  %%xmm5\n"
                "psrldq $6,  %%xmm6\n"

                "pmaxub %%xmm1, %%xmm0\n"       // Takes the maximum values in the entire kernel and stores it in xmm0
                "pmaxub %%xmm2, %%xmm0\n"
                "pmaxub %%xmm3, %%xmm0\n"
                "pmaxub %%xmm4, %%xmm0\n"
                "pmaxub %%xmm5, %%xmm0\n"
                "pmaxub %%xmm6, %%xmm0\n"

                                                // -----------------------
                                                // Applies the same previous steps to get the minimum in the entire kernel
                "movdqa %%xmm7, %%xmm1\n"
                "movdqa %%xmm7, %%xmm2\n"
                "movdqa %%xmm7, %%xmm3\n"
                "movdqa %%xmm7, %%xmm4\n"
                "movdqa %%xmm7, %%xmm5\n"
                "movdqa %%xmm7, %%xmm6\n"



                "psrldq $1,  %%xmm1\n"
                "psrldq $2,  %%xmm2\n"
                "psrldq $3,  %%xmm3\n"
                "psrldq $4,  %%xmm4\n"
                "psrldq $5,  %%xmm5\n"
                "psrldq $6,  %%xmm6\n"



                "pminub %%xmm1, %%xmm7\n"
                "pminub %%xmm2, %%xmm7\n"
                "pminub %%xmm3, %%xmm7\n"
                "pminub %%xmm4, %%xmm7\n"
                "pminub %%xmm5, %%xmm7\n"
                "pminub %%xmm6, %%xmm7\n"

                                                // -----------------------
                "psubb  %%xmm7, %%xmm0\n"       // Applies max - min
                "movdqu %%xmm0, (%%rsi)\n"      // Writes on the output

                "add $10, %%rsi\n"				// Passes to the next 14 bytes of inputs and output
                "add $10, %%rax\n"
                "add $10, %%rbx\n"
                "add $10, %%rdi\n"
                "add $10, %%r8\n"
                "add $10, %%r9\n"
                "add $10, %%r10\n"
                "add $10, %%r11\n"

                "sub $1, %%r12 \n"	               // iterator -= 1
                "jnz loop7 \n"					// jump to loop label if iterator > 0
            :
            : [out]"m" (dst), [in]"m" (src), [it]"m" (iter),[in1]"m" (src1), [in2]"m" (src2), [in3]"m" (src3), [in4]"m" (src4), [in5]"m" (src5), [in6]"m" (src6)
            :  "rsi", "rax", "rbx", "rdi", "r8", "r9", "r10", "r11", "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7", "r12"
            );
}



// === END Program === //

