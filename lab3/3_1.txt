// Multiplication algorithm :
// 		with shift and add method
//		The method is done under a unsigned logic.
//
// by 	STEFAN Vlad 
//		LENGELE Celestin 
//		BERTOUX Antoine 
//		VERHEEKE Sacha

// --- Initialisation --- //
movi 1,0x7fff						// operand 1 : x
movi 2,0x0007						// operand 2 : y
movi 5,0x8000						// reg5 = 1000 0000 0000 0000 (bit mask to take MSB)
movi 6,0x0001						// reg6 = 0000 0000 0000 0001 (counter used to count 16 iterations) will be equal to 0 after 16 shifts


// Loop of 16 iterations
// Add (with carry bit from reg3 to reg4)
// End prog if counter = 0
// Shift (with carry bit from reg3 to reg4)
// End loop

// Total : 16 additions and 15 shifts


// If reg1 or reg2 = zero
beq 0,1,end_prog
beq 0,2,end_prog

// --- Loop --- //
begin_loop: add 6,6,6				// Counter of 16 iterations (the 16 bits of reg2) (In 16 shifts reg6 will be equals to 0x0000)
	nand 7,5,2
	nand 7,7,7 						// reg7 <- MSB reg2

	beq 7,5,plus 					// If MSB reg2 = 1 (If MSB y = 1)
	beq 0,0,s 						// Else

	plus: add 3,3,1,carry_plus		// Shift reg3, if overflow carry bit in reg4
	beq 0,0,s
	carry_plus: addi 4,4,1			// Add 1 to reg4 (carry bit)
	s: beq 0,6,end_prog				// If counter = 0 -> end program
		add 4,4,4					// Shift of reg4
		add 3,3,3,carry_s			// Shift of reg3, if overflow carry bit in reg4

		beq 0,0,end_loop			// Else (MSB reg3 = 0, no need of carrying bit)

		carry_s: addi 4,4,1			// Add 1 to reg4

	end_loop: add 2,2,2				// End loop -> take the next bit of reg2
		beq 0,0,begin_loop			// Back to the head of the loop

// --- End Loop --- //

// --- End Program --- //
end_prog: halt						// THE END ? 