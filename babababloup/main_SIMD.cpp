/* 
The program thresholds a raw input image and outputs a binary image.

It takes as argument the raw image (input), the expected name for the binary (output) 
    and the dimensions of the input image (width, height).
It outputs all the values greater than the threshold value as 1 and the values that are below as 0.

Program was made for the course ELEC-H473 - Microprocessor Architecture

The threshold was made in assembly

March 2021
authors :
    Antoine Bertoux
    Celestin Lengele
    Vlad Stefan
    Sacha Verheeke
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <vector>

int main(int argc, char** argv)
{   
    // === Initialisation === //

    int W, H; // Width, Height of the raw image
    int ret_val; // RETurn VALue to test the well using of the functions
    u_int8_t threshold = 128; // threshold value (initial value = 128)
    int threshold_shift = 128; // threshold i to the code
    int iter;
    char* img_path;
    char* out_path;
    // == Error Handling == //
    if(argc < 5 || argc > 6)
    {
        fprintf(stderr, "Incorrect use of the program : %s\n", argv[0]);
        fprintf(stderr, "The correct use is : %s <input> <dest> <width> <height> [threshold = 128]", argv[0]);
        return 1;
    }
    else if(argc == 6)
    {
        ret_val = sscanf(argv[5], "%d", &threshold);
        // Error handling
        if(ret_val == EOF)
        {
            fprintf(stderr, "The treshold argument is supposed to be a number.\n");
            fprintf(stderr, "Incorrect use of the program : %s\n", argv[0]);
            fprintf(stderr, "The correct use is : %s <input> <dest> <width> <height> <threshold>", argv[0]);
            return 1;
        }
        if(threshold < 0 || threshold > 255)
        {
            fprintf(stderr, "WARNING : The treshold is supposed to be an integer between 0 and 255\n");
            fprintf(stderr, "The received integer will be positive modulo 255");
        }
    }

    img_path = argv[1];
    out_path = argv[2];
    
    // == Error Handling == //
    if(sscanf(argv[3], "%d", &W) == EOF || sscanf(argv[4], "%d", &H) ==  EOF || W < 0 || H < 0)
    {
        fprintf(stderr, "Both width and height should be positive integers");
        return 1;
    }
    iter = W * H / 16;
    threshold -= 128;

    // === END Initialisation === //


    // === Begin Core Program === //

    // Image size fixed (or from header)
    unsigned char *src; // Pointers to arrays source and destination
    unsigned char *dst;

    clock_t start, end;
    float time;

    for(int j=0; j<20; j++)
    {
        src = (unsigned char *) malloc (W * H * sizeof(unsigned char));
        dst = (unsigned char *) malloc (W * H * sizeof(unsigned char));

        unsigned char* src_copy = src;
        unsigned char* dest_copy = dst;

        // Check if enough memory, although with virtual memory less like to happen
        if (src == NULL || dst == NULL) {
            printf ("Out of memory!");
            exit (1);
        }
        // fp1 reading, fp2 writing
        FILE *fp1 = fopen(img_path,"rb");
        FILE *fp2 = fopen(out_path,"wb");

        if (fp1 == NULL) {
            // and we close the pointer
            printf("Cannot open specified file!");
            exit(1);
        }

        // == Threshold == //

        start = clock();

        __asm__(
        "mov %[out], %%rsi\n"               // rsi -> output
        "mov %[in] , %%rax\n"               // rax -> input
        "mov %[it] , %%rcx\n"               // rcx -> iterator
        "movdqu %[t],   %%xmm7\n"           // mov 16 bytes by 16 bytes
        "vpbroadcastb %%xmm7 ,%%xmm7\n"     // copy the first byte to all the 16 bytes of the register
        "movdqu %[ts] , %%xmm3\n"           // xmm3 -> treshhold value
        "vpbroadcastb %%xmm3, %%xmm3\n"     // copy the first byte to all the 16 bytes of the register

        "loop: \n"
            "movdqa (%%rax), %%xmm0 \n"     // mov input in xmm0
            "paddb %%xmm3, %%xmm0\n"        // xmm0 = input - 128
            "pcmpgtb %%xmm7, %%xmm0 \n"     // if xmm0 > xmm7 -> xmm0 = 1111 1111 otherwise = 0000 0000

            "movdqa %%xmm0, (%%rsi) \n"     // xmm0 -> output
            "add $16, %%rsi\n"              // pass to the next 16 bytes of output
            "add $16, %%rax\n"              // pass to the next 16 bytes of input
            "subq $1, %[it] \n"              // iterator -= 1
            "jnz loop; \n"                  // jump to loop label if iterator > 0
        :
        : [out]"m" (dst), [in]"m" (src), [it]"m" (iter), [t]"m" (threshold), [ts]"m" (threshold_shift)
        :
        );

        // == END Threshold == //

        end = clock();                                  // END BENCHMARK

        time = (float) (end - start)/CLOCKS_PER_SEC;    // Time spent

        printf("Time spent: %f\n", time);               // PRINT BENCHMARK

        fwrite(dst, sizeof(unsigned char), W * H, fp2);

        free(src); // avoid memory leaks
        free(dst);

        fclose(fp1);
        fclose(fp2);
    }

    return 0;
}

// === END Program === //



